import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;

public class Tests {


//    @BeforeTest
//    public static void openUrl() {
//        open("http://localhost:8088/#/");
//    }

    @Test
    public static void testCase1() throws InterruptedException {
        open("http://localhost:8088/#/");
        Assert.assertEquals(url(), "http://localhost:8088/#/");
    }

    @Test
    public static void testCase2_1() {
        open("http://localhost:8088/#/");
        SelenideElement filterId = $("div.sort:nth-child(1)");
        filterId.click();
        SelenideElement firstPositionInTable = $("div.tr:nth-child(1) > div:nth-child(1) > span:nth-child(1)");
        int currentnumberValue = Integer.parseInt(firstPositionInTable.getText());
        Assert.assertTrue(currentnumberValue > 0);
    }

    @Test
    public static void testCase2_2() throws InterruptedException {
        open("http://localhost:8088/#/");
        SelenideElement filterById = $("div.sort:nth-child(1)");
        filterById.click();
        filterById.click();
        SelenideElement firstPositionInTableId = $("div.tr:nth-child(1) > div:nth-child(1) > span:nth-child(1)");
        firstPositionInTableId.waitUntil(Condition.visible, 10000);
        Assert.assertEquals(firstPositionInTableId.getText(), "0");
    }

    @Test
    public static void testCase3_1() throws InterruptedException {
        open("http://localhost:8088/#/");
        SelenideElement filterByName = $("div.sort:nth-child(2)");
        filterByName.click();
        SelenideElement firstPositionInTableName = $("div.tr:nth-child(1) > div:nth-child(2) > span:nth-child(1)");
        firstPositionInTableName.waitUntil(Condition.visible, 10000);
        Assert.assertEquals("Задание & 999", firstPositionInTableName.getText());
    }

    @Test
    public static void testCase3_2() throws InterruptedException {
        open("http://localhost:8088/#/");
        SelenideElement filterByName = $("div.sort:nth-child(2)");
        filterByName.click();
        filterByName.click();
        SelenideElement firstPositionInTableName = $("div.tr:nth-child(1) > div:nth-child(2) > span:nth-child(1)");
        firstPositionInTableName.waitUntil(Condition.visible, 10000);
        Assert.assertEquals("Задание 0", firstPositionInTableName.getText());
    }

    @Test
    public static void testCase4_1() throws InterruptedException {
        open("http://localhost:8088/#/");
        SelenideElement filterByType = $("div.sort:nth-child(4)");
        filterByType.click();
        SelenideElement firstPositionInTableType = $("div.tr:nth-child(1) > div:nth-child(4) > span:nth-child(1)");
        firstPositionInTableType.waitUntil(Condition.visible, 10000);
        Assert.assertEquals("Срочное", firstPositionInTableType.getText());
    }

    @Test
    public static void testCase4_2() throws InterruptedException {
        open("http://localhost:8088/#/");
        SelenideElement filterByType = $("div.sort:nth-child(4)");
        filterByType.click();
        filterByType.click();
        SelenideElement firstPositionInTableType = $("div.tr:nth-child(1) > div:nth-child(4) > span:nth-child(1)");
        firstPositionInTableType.waitUntil(Condition.visible, 10000);
        Assert.assertEquals(firstPositionInTableType.getText(), "Ручное");
    }

    @Test
    public static void testCase5_1() throws InterruptedException {
        open("http://localhost:8088/#/");
        SelenideElement filterByStatus = $("div.sort:nth-child(5)");
        filterByStatus.click();
        SelenideElement firstPositionInTableStatus = $("div.tr:nth-child(1) > div:nth-child(5) > span:nth-child(1)");
        firstPositionInTableStatus.waitUntil(Condition.visible, 10000);
        Assert.assertEquals(firstPositionInTableStatus.getText(), "RUNNING");
    }

    @Test
    public static void testCase5_2() throws InterruptedException {
        open("http://localhost:8088/#/");
        SelenideElement filterByStatus = $("div.sort:nth-child(5) > span:nth-child(1)");
        filterByStatus.click();
        filterByStatus.click();
        SelenideElement firstPositionInTableStatus = $("div.tr:nth-child(1) > div:nth-child(5) > span:nth-child(1)");
        firstPositionInTableStatus.waitUntil(Condition.visible, 10000);
        Assert.assertEquals("CANCELED", firstPositionInTableStatus.getText());
    }

    @Test
    public static void testCase6_1() throws InterruptedException {
        open("http://localhost:8088/#/");
        SelenideElement filterByProgressBar = $("div.sort:nth-child(6) > span:nth-child(1)");
        filterByProgressBar.click();
        Thread.sleep(2000);
        SelenideElement firstPositionInTableProgressBar = $("div.tr:nth-child(1) > div:nth-child(6) > span:nth-child(1)");
        Assert.assertEquals(firstPositionInTableProgressBar.getText(), "100");
    }

    @Test
    public static void testCase6_2() throws InterruptedException {
        open("http://localhost:8088/#/");
        SelenideElement filterByProgressBar = $("div.sort:nth-child(6) > span:nth-child(1)");
        filterByProgressBar.click();
        filterByProgressBar.click();
        Thread.sleep(2000);
        SelenideElement firstPositionInTableProgressBar = $("div.tr:nth-child(1) > div:nth-child(6) > span:nth-child(1)");
        Assert.assertEquals(firstPositionInTableProgressBar.getText(), "0");
    }

}